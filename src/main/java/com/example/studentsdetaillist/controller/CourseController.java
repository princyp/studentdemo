package com.example.studentsdetaillist.controller;



import com.example.studentsdetaillist.Exception.BadDataException;
import com.example.studentsdetaillist.dto.*;
import com.example.studentsdetaillist.service.CourseService;
import com.example.studentsdetaillist.utils.AppResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * User Apis.
 */
@RestController
@RequestMapping(value = "/course")
public class CourseController {
    private CourseService courseService;

    public CourseController(final CourseService courseService) {
        this.courseService = courseService;
    }

    /**
     * @param createRequest createRequest
     * @return createAccount


     */
    @ApiOperation("")
    @PostMapping("/addcourse")
    public AppResponse<CourseResDTO> createAccount(@Valid @RequestBody final CourseReqDTO createRequest) {

        if (createRequest.getLocation() == null ) {
            throw new BadDataException("Please provide Coursename");
        }

        CourseResDTO response = courseService.createCourse(createRequest);
        if (response != null) {
            return AppResponse.<CourseResDTO>builder().build().<CourseResDTO>builder()
                    .data(response)
                    .message("Teacher registration process sucessfully!")
                    .success(true)
                    .build();
        }
        return AppResponse.<CourseResDTO>builder()
                .message("Error creating teacher account!")
                .success(false)
                .build();
    }
    /**
     * @param request request.
     * @return PatientUpdateResponseDTO.
     */
    @ApiOperation("")
    @PutMapping(value = "/courseEdit")
    public AppResponse<CourseUpdResDTO> courseEdit(@RequestBody final CourseUpdReqDTO request) {
        CourseUpdResDTO response = courseService.courseEdit(request);
        if (response != null) {
            return AppResponse.<CourseUpdResDTO>builder()
                    .data(response)
                    .message("Course  details updated successfully!")
                    .success(true)
                    .build();
        }
        return AppResponse.<CourseUpdResDTO>builder()
                .message("Course update went wrong, try again later!")
                .success(false)
                .build();
    }

    /**
     * @
     * @return CourseListResDTO.
     */
    @ApiOperation("get all course list")
    @GetMapping(value = "/course/list")
    public AppResponse<CourseListResDTO> getAllUsers(
            @RequestParam(value = "query", required = false, defaultValue = "") final String query,
            @RequestParam(value = "filterBy", required = false, defaultValue = "") final String filterBy,
            @RequestParam(value = "filtervalue", required = false, defaultValue = "") final String filtervalue,
            @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") final Integer size

    ) {

        CourseListResDTO response = courseService.listAllCourse(query,filterBy,filtervalue,page,size);
        if (response.getCourseProfileList().size() != 0) {
            return AppResponse.<CourseListResDTO>builder()
                    .data(response)
                    .message("student list fetched successfully")
                    .success(true)
                    .build();
        }
        return AppResponse.<CourseListResDTO>builder()
                .data(response)
                .success(false)
                .message("No content found!")
                .build();
    }
    /**
     * @param courseId    userExternalId
     * @return getMedicineDetails
     */
    @ApiOperation(" course id details")
    @GetMapping("/courseDetails")
    public AppResponse<CourseIDDetailsDTO> getMedicineDetails(@ApiParam(value = "Enter the courseId", required = false)
                                                                @RequestParam(value = "courseId", required = false) final Long courseId){


        CourseIDDetailsDTO response = courseService.getCourseDetails(courseId);
        if (response.getCourseId()!=0) {
            return AppResponse.<CourseIDDetailsDTO>builder()
                    .data(response)
                    .message("course details fetch successfully")
                    .success(true)
                    .build();
        } else {
            throw new BadDataException("failed to course details details details!");
        }
    }
    /**
     * @param courseExternalId courseExternalId.
     * @return deleteStudent.
     */
    @ApiOperation("delete course by course external id")
    @DeleteMapping(value = "/deletecourse")
    public AppResponse<CourseStatusDTO> deleteId
    (@ApiParam(value = "student id which from which student table"
            + " object will delete from database table", required = true)
     @RequestParam final Long courseExternalId) {
        if (courseExternalId == null && courseExternalId ==0) {
            throw new BadDataException("Please provide address external id");
        }

        CourseStatusDTO response = courseService.deleteCourseId(courseExternalId);
        if (response != null) {
            return AppResponse.<CourseStatusDTO>builder()
                    .data(response)
                    .message("Requested course is deleted successfully!")
                    .success(true)
                    .build();
        }
        return AppResponse.<CourseStatusDTO>builder()
                .message("Oops something went wrong, try again later!")
                .success(false)
                .build();
    }
}

