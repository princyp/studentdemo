package com.example.studentsdetaillist.controller;

import com.example.studentsdetaillist.Exception.BadDataException;
import com.example.studentsdetaillist.dto.*;
import com.example.studentsdetaillist.service.TeacherService;
import com.example.studentsdetaillist.utils.AppResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * User Apis.
 */
@RestController
@RequestMapping(value = "/user")
public class TeacherController {

    private TeacherService teacherService;

    public TeacherController(final TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    /**
     * @param createRequest createRequest
     * @return createAccount
     */
    @ApiOperation("")
    @PostMapping("/createAccount")
    public AppResponse<TeacherResDTO> createAccount(@Valid @RequestBody final TeacherReqDTO createRequest) {

        if (createRequest.getTeacherName() == null) {
            throw new BadDataException("Please provide Teachername");
        }

        TeacherResDTO response = teacherService.createTeacher(createRequest);
        if (response != null) {
            return AppResponse.<TeacherResDTO>builder().build().<TeacherResDTO>builder()
                    .data(response)
                    .message("Teacher registration process sucessfully!")
                    .success(true)
                    .build();
        }
        return AppResponse.<TeacherResDTO>builder()
                .message("Error creating teacher account!")
                .success(false)
                .build();
    }

    /**
     * @param request request.
     * @return PatientUpdateResponseDTO.
     */
    @ApiOperation("")
    @PutMapping(value = "/teacherEdit")
    public AppResponse<TeacherUpResDTO> courseEdit(@RequestBody final TeacherUpReqDTO request) {
        TeacherUpResDTO response = teacherService.editTeacher(request);
        if (response != null) {
            return AppResponse.<TeacherUpResDTO>builder()
                    .data(response)
                    .message("Teacher   details updated successfully!")
                    .success(true)
                    .build();
        }
        return AppResponse.<TeacherUpResDTO>builder()
                .message("Course update went wrong, try again later!")
                .success(false)
                .build();
    }

    /**
     * @return StudentsListResDTO.
     * @
     */
    @ApiOperation("get all teacher list")
    @GetMapping(value = "/teacher/list")
    public AppResponse<TeacherListResDTO> getAllUsers(
            @RequestParam(value = "query", required = false, defaultValue = "") final String query,
            @RequestParam(value = "filterBy", required = false, defaultValue = "") final String filterBy,
            @RequestParam(value = "filtervalue", required = false, defaultValue = "") final String filtervalue,
            @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") final Integer size

    ) {

        TeacherListResDTO response = teacherService.listAllTeacher(query, filterBy, filtervalue, page, size);
        if (response.getTeacherProfileList().size() != 0) {
            return AppResponse.<TeacherListResDTO>builder()
                    .data(response)
                    .message("teacher list fetched successfully")
                    .success(true)
                    .build();
        }
        return AppResponse.<TeacherListResDTO>builder()
                .data(response)
                .success(false)
                .message("No content found!")
                .build();
    }

    /**
     * @param teacherId userExternalId
     * @return getMedicineDetails
     */
    @ApiOperation(" teacher id details")
    @GetMapping("/teacherDetails")
    public AppResponse<TeacherIDDetailsDTO> getMedicineDetails(@ApiParam(value = "Enter the teacherId", required = false)
                                                              @RequestParam(value = "teacherId", required = false) final Long teacherId) {


        TeacherIDDetailsDTO response = teacherService.getTeacherDetails(teacherId);
        if (response.getTeacherId()!=0) {
            return AppResponse.<TeacherIDDetailsDTO>builder()
                    .data(response)
                    .message("teacher details fetch successfully")
                    .success(true)
                    .build();
        } else {
            throw new BadDataException("failed to fetch teacher details details!");
        }
    }
    /**
     * @param teacherExternalId teacherExternalId.
     * @return deleteStudent.
     */
    @ApiOperation("delete teacher by teacher external id")
    @DeleteMapping(value = "/deleteteacher")
    public AppResponse<TeacherStatusDTO> deleteId
    (@ApiParam(value = "teacher id which from which teacher table"
            + " object will delete from database table", required = true)
     @RequestParam final Long teacherExternalId) {
        if (teacherExternalId == null && teacherExternalId ==0) {
            throw new BadDataException("Please provide teacher external id");
        }

        TeacherStatusDTO response = teacherService.deleteTeacherId(teacherExternalId);
        if (response != null) {
            return AppResponse.<TeacherStatusDTO>builder()
                    .data(response)
                    .message("Requested student is deleted successfully!")
                    .success(true)
                    .build();
        }
        return AppResponse.<TeacherStatusDTO>builder()
                .message("Oops something went wrong, try again later!")
                .success(false)
                .build();
    }
}

