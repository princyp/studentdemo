package com.example.studentsdetaillist.controller;

import com.example.studentsdetaillist.Exception.BadDataException;
import com.example.studentsdetaillist.dto.*;
import com.example.studentsdetaillist.service.StudentService;
import com.example.studentsdetaillist.utils.AppResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Objects;


/**
 * User Apis.
 */
@RestController
@RequestMapping(value = "/student")
public class StudentController {

    private StudentService studentService;

    public StudentController(final StudentService studentService) {
        this.studentService = studentService;
    }
    @ApiOperation("")
    @PostMapping("/studentAccount")
    public AppResponse<StudentResDTO> createAccount(@Valid @RequestBody final StudentReqDTO createRequest) {

        if (createRequest.getCity() == null ) {
            throw new BadDataException("Please provide studentdetails");
        }

        StudentResDTO response = studentService.createStudent(createRequest);
        if (response != null) {
            return AppResponse.<StudentResDTO>builder().build().<StudentResDTO>builder()
                    .data(response)
                    .message("student registration process sucessfully!")
                    .success(true)
                    .build();
        }
        return AppResponse.<StudentResDTO>builder()
                .message("Error creating teacher account!")
                .success(false)
                .build();
    }
    /**
     * @param request request.
     * @return PatientUpdateResponseDTO.
     */
    @ApiOperation("")
    @PutMapping(value = "/studentEdit")
    public AppResponse<StudentUpResDTO> courseEdit(@RequestBody final StudentUpReqDTO request) {
        StudentUpResDTO response = studentService.editStudent(request);
        if (response != null) {
            return AppResponse.<StudentUpResDTO>builder()
                    .data(response)
                    .message("Teacher   details updated successfully!")
                    .success(true)
                    .build();
        }
        return AppResponse.<StudentUpResDTO>builder()
                .message("Course update went wrong, try again later!")
                .success(false)
                .build();
    }

    /**
     * @
     * @return StudentsListResDTO.
     */
    @ApiOperation("get all students list")
    @GetMapping(value = "/students/list")
    public AppResponse<StudentsListResDTO> getAllUsers(
            @RequestParam(value = "query", required = false, defaultValue = "") final String query,
            @RequestParam(value = "filterBy", required = false, defaultValue = "") final String filterBy,
            @RequestParam(value = "filtervalue", required = false, defaultValue = "") final String filtervalue,
          @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
         @RequestParam(value = "size", required = false, defaultValue = "10") final Integer size

    ) {

     StudentsListResDTO response = studentService.listAllStudents(query,filterBy,filtervalue,page,size);
        if (response.getStudentProfileList().size() != 0) {
            return AppResponse.<StudentsListResDTO>builder()
                    .data(response)
                    .message("course list fetched successfully")
                    .success(true)
                    .build();
        }
        return AppResponse.<StudentsListResDTO>builder()
                .data(response)
                .success(false)
                .message("No content found!")
                .build();
    }
    /**
     * @param studentId    userExternalId
     * @return getMedicineDetails
     */
    @ApiOperation(" student id details")
    @GetMapping("/studentDetails")
    public AppResponse<StudentsIDDetailsDTO> getMedicineDetails(@ApiParam(value = "Enter the studentId", required = false)
                                                                  @RequestParam(value = "studentId", required = false) final Long studentId){


        StudentsIDDetailsDTO response = studentService.getStudentsDetails(studentId);
        if (response.getStudentId()!=0) {
            return AppResponse.<StudentsIDDetailsDTO>builder()
                    .data(response)
                    .message("student details fetch successfully")
                    .success(true)
                    .build();
        } else {
            throw new BadDataException("failed to fetch student details details!");
        }
    }
    /**
     * @return studentSearch
     */
    @ApiOperation("search by student name,city,and standard")
    @GetMapping("/studentSearch")
    public AppResponse<List<StudentSearchResDTO>> studentSearch(
            @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") final Integer size,
            @RequestParam(value = "sort", required = false, defaultValue = "DESC") final Sort.Direction sort,
            @ApiParam(value = "search by student name", required = false)
            @RequestParam(value = "search", required = false) final String search,
          @ApiParam(value = "search by student name", required = false)
          @RequestParam(value = "city", required = false) final String city,
            @ApiParam(value = "search by standard", required = false)
            @RequestParam(value = "standard", required = false) final String standard)
         {

        List<StudentSearchResDTO> studentSearchResponse = studentService.findStudentByCriteria(page, size, sort, search,city,standard);
        if (studentSearchResponse != null) {
            return AppResponse.<List<StudentSearchResDTO>>builder()
                    .data(studentSearchResponse)
                    .message("Successfully search the data")
                    .success(true)
                    .build();
        }
        return AppResponse.<List<StudentSearchResDTO>>builder()
                .message("Something went wrong,please try again later!")
                .success(false)
                .build();
    }

    /**
     * @return CourseSearch
     */
    @ApiOperation("category students  on Course")
    @GetMapping("/CourseSearch")
    public AppResponse<List<CourseSearchResDTO>> studentSearch(
            @RequestParam(value = "page", required = false, defaultValue = "0") final Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") final Integer size,
            @RequestParam(value = "sort", required = false, defaultValue = "DESC") final Sort.Direction sort,

            @ApiParam(value = "search by course ", required = false)
            @RequestParam(value = "courseName", required = false) final String courseName)
    {
        List<CourseSearchResDTO> studentSearchResponse = studentService.findStudentByCourse(page, size, sort, courseName);
        if (studentSearchResponse != null) {
            return AppResponse.<List<CourseSearchResDTO>>builder()
                    .data(studentSearchResponse)
                    .message("Successfully search the data")
                    .success(true)
                    .build();
        }
        return AppResponse.<List<CourseSearchResDTO>>builder()
                .message("Something went wrong,please try again later!")
                .success(false)
                .build();
    }

    /**
     * @param studentExternalId studentdelete.
     * @return deleteStudent.
     */
    @ApiOperation("delete student by student external id")
    @DeleteMapping(value = "/deletestudent")
    public AppResponse<StudentStatusDTO> deleteId
           (@ApiParam(value = "student id which from which student table"
            + " object will delete from database table", required = true)
            @RequestParam final Long studentExternalId) {
        if (studentExternalId == null && studentExternalId ==0) {
            throw new BadDataException("Please provide address external id");
        }

        StudentStatusDTO response = studentService.deleteStudentId(studentExternalId);
        if (response != null) {
            return AppResponse.<StudentStatusDTO>builder()
                    .data(response)
                    .message("Requested student is deleted successfully!")
                    .success(true)
                    .build();
        }
        return AppResponse.<StudentStatusDTO>builder()
                .message("Oops something went wrong, try again later!")
                .success(false)
                .build();
    }

}
