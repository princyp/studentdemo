package com.example.studentsdetaillist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentsdetaillistApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentsdetaillistApplication.class, args);
    }

}
