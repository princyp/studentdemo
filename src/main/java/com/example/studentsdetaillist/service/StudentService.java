package com.example.studentsdetaillist.service;
import com.example.studentsdetaillist.dto.*;
import com.example.studentsdetaillist.entity.Student;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

/**
 * The StudentService.
 */
public interface StudentService {

    /**
     * To createStudent.
     *
     * @param student student
     * @return Student
     */
    StudentResDTO createStudent(StudentReqDTO student);
    StudentUpResDTO editStudent(StudentUpReqDTO studentreq);
    /**
     * * @return StudentsListResDTO.
     */
    StudentsListResDTO listAllStudents(String query, String filterBy, String filtervalue,Integer page,Integer size);
    /**

     * @param studentId  studentId
     * @return getMedicineDosage
     */
    StudentsIDDetailsDTO getStudentsDetails(Long studentId);
    List<StudentSearchResDTO> findStudentByCriteria(int page, int size, Sort.Direction sort, String search, String city, String standard);

    /**
     * @param page
     * @param size
     * @param sort
     * @param courseName
     * @return StudentsearchResDTO
     */
    List<CourseSearchResDTO> findStudentByCourse(int page, int size, Sort.Direction sort,String courseName);
    /**
     * @param addressExternalId addressExternalId.
     * @return deleteAddress.
     */
    StudentStatusDTO deleteStudent(String addressExternalId);
    /**
     * @param studentExternalId addressExternalId.
     * @return deleteAddress.
     */
    StudentStatusDTO deleteStudentId(Long studentExternalId);
    /**

     * @return findByUserIdAndDeletedFalse.
     */

}
