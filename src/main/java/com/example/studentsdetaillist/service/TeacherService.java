package com.example.studentsdetaillist.service;

import com.example.studentsdetaillist.dto.*;

/**
 * The TeacherService.
 */
public interface TeacherService {

    /**
     * To createTeacher.
     *
     * @param teacher teacher
     * @return Teacher
     */
    TeacherResDTO createTeacher(TeacherReqDTO teacher);

    TeacherUpResDTO editTeacher(TeacherUpReqDTO teacherreq);
    /**
     * * @return TeacherListResDTO.
     */
    TeacherListResDTO listAllTeacher(String query, String filterBy, String filtervalue, Integer page, Integer size);
    TeacherIDDetailsDTO getTeacherDetails(Long courseId);
    /**
     * @param teacherExternalId teacherExternalId.
     * @return deleteAddress.
     */
    TeacherStatusDTO deleteTeacherId(Long teacherExternalId);

}

