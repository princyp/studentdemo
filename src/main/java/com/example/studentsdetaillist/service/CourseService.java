package com.example.studentsdetaillist.service;

import com.example.studentsdetaillist.dto.*;

public interface CourseService {
        /**
         * The StudentService.
         */
        /**
         * @param createCourse createCourse
         * @return CourseResDTO
         */
        CourseResDTO createCourse(CourseReqDTO createCourse);


        CourseUpdResDTO courseEdit(CourseUpdReqDTO courseupreq);
        /**
         * * @return CourseListResDTO.
         */
        CourseListResDTO listAllCourse(String query, String filterBy, String filtervalue,Integer page,Integer size);
        /**

         * @param courseId  courseId
         * @return getMedicineDosage
         */
        CourseIDDetailsDTO getCourseDetails(Long courseId);
        /**
         * @param courseExternalId courseExternalId.
         * @return deleteAddress.
         */
        CourseStatusDTO deleteCourseId(Long courseExternalId);
        }

