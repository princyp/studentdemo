package com.example.studentsdetaillist.service.serviceImplementation;

import com.example.studentsdetaillist.Exception.BadDataException;
import com.example.studentsdetaillist.dto.*;

import com.example.studentsdetaillist.entity.*;

import com.example.studentsdetaillist.repository.CourseRepository;

import com.example.studentsdetaillist.repository.TeacherRepository;
import com.example.studentsdetaillist.service.CourseService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CourseServiceImp implements CourseService {
    private final CourseRepository courseRepository;
    private final TeacherRepository teacherRepository;

    public CourseServiceImp(CourseRepository courseRepository,
                            TeacherRepository teacherRepository) {
        this.courseRepository = courseRepository;
        this.teacherRepository = teacherRepository;

    }

    @Override
    public CourseResDTO createCourse(CourseReqDTO coursereq) {
        Course course = new Course();
        course.setLocation(coursereq.getLocation());
        course.setCourseName(coursereq.getCourseName());
        Teacher teachers1 = teacherRepository.findByTeacherName(coursereq.getTeacher());
        course.setTeacher(teachers1);
        Course courses = courseRepository.save(course);
        CourseResDTO courseResDTO = new CourseResDTO();
        courseResDTO.setLocation(courses.getLocation());
        return courseResDTO;
    }

    @Override
    public CourseUpdResDTO courseEdit(CourseUpdReqDTO courseupdatereq) {
        Optional<Course> courseGet = courseRepository.findByCourseId(courseupdatereq.getCourseId());
        if (courseGet.isPresent()) {
            courseGet.get().setCourseName(courseupdatereq.getCourseName());
            courseGet.get().setLocation(courseupdatereq.getLocation());
            Teacher teacher = teacherRepository.findByTeacherName(courseupdatereq.getTeacher());

            if(Objects.isNull(teacher)){
                throw new BadDataException("teacher not found");
            }
            courseGet.get().setTeacher(teacher);
            Course saveco= courseRepository.save(courseGet.get());
            CourseUpdResDTO courseUpdResDTO = new CourseUpdResDTO();
            courseUpdResDTO.setCourseId(saveco.getCourseId());
            courseUpdResDTO.setCourseName(saveco.getCourseName());
            return courseUpdResDTO;
        } else {
            throw new BadDataException(" course found!");
        }

    }

    @Override
    public CourseListResDTO listAllCourse(String query, String filterBy, String filtervalue, Integer page, Integer size) {
        CourseListResDTO listResponse = new CourseListResDTO();
        Page<Course> courses;
        List<CourseProfileListDTO> courseLists = new ArrayList<>();

        String fieldSort = "courseName";
        Integer currentPage = page;
        Integer pageSize = size;
        Sort.Order sortOrder = new Sort.Order(Sort.Direction.ASC, fieldSort);
        PageRequest pageRequest = PageRequest.of(currentPage, pageSize, Sort.by(sortOrder));
        courses = courseRepository.findAll(pageRequest);


        if (courses != null && courses.getContent().size() > 0) {
            courses.getContent().forEach(userObj -> {
                CourseProfileListDTO courseProfileList = getPaginatedStudentList(userObj);
                        if (courseProfileList != null) {
                            courseLists.add(courseProfileList);
                        }
                    }
            );            }
        if (courseLists.size() > 0) {
            listResponse.setCurrentPages(page);
            listResponse.setTotalPages(courses.getTotalPages());
            listResponse.setCourseProfileList(courseLists);
            listResponse.setTotal(courses.getTotalElements());

            return listResponse;
        }
        else {
            listResponse.setCurrentPages(0);
            listResponse.setTotalPages(0);
            listResponse.setCourseProfileList(courseLists);
            listResponse.setTotal(0L);
            return listResponse;


        }
    }
    private CourseProfileListDTO getPaginatedStudentList(Course userObj) {
       CourseProfileListDTO courseProfileList = new CourseProfileListDTO();
        courseProfileList.setCourseName(userObj.getCourseName());
        courseProfileList.setLocation(userObj.getLocation());
        courseProfileList.setTeacher(userObj.getTeacher().getTeacherName());
        return courseProfileList;
    }

    @Override
    public CourseIDDetailsDTO getCourseDetails(Long courseId) {
        CourseIDDetailsDTO courseIDDetailsDTO = new CourseIDDetailsDTO();
        Optional<Course> course = courseRepository.findById(courseId);

        if(course.isPresent()){
            courseIDDetailsDTO.setCourseId(course.get().getCourseId());
            courseIDDetailsDTO.setCourseName(course.get().getCourseName());
            courseIDDetailsDTO.setLocation(course.get().getLocation());
//            courseIDDetailsDTO.setStandName(course.get().getStandardName());
        }
        return courseIDDetailsDTO;
    }

    @Override
    public CourseStatusDTO deleteCourseId(Long courseExternalId) {
        if (courseExternalId !=0) {
            Optional<Course> courseGet = courseRepository.findByCourseIdAndDeletedFalse(courseExternalId);
            if (courseGet.isPresent()) {
                courseGet.get().setDeleted(true);
                courseRepository.save(courseGet.get());


                CourseStatusDTO response = new CourseStatusDTO();
                response.setStatus("DELETED");
                return response;
            } else {
                throw new BadDataException("There is no course corresponding externalId");
            }
        } else {
            throw new BadDataException("Please provide course external id");
        }
    }
    }




