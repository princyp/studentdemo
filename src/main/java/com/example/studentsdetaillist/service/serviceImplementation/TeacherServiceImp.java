package com.example.studentsdetaillist.service.serviceImplementation;
import com.example.studentsdetaillist.Exception.BadDataException;
import com.example.studentsdetaillist.dto.*;
import com.example.studentsdetaillist.entity.Course;
import com.example.studentsdetaillist.entity.Standard;
import com.example.studentsdetaillist.entity.Student;
import com.example.studentsdetaillist.entity.Teacher;
import com.example.studentsdetaillist.repository.CourseRepository;
import com.example.studentsdetaillist.repository.StandardRepository;
import com.example.studentsdetaillist.repository.TeacherRepository;
import com.example.studentsdetaillist.service.TeacherService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.sun.xml.bind.api.impl.NameConverter.standard;

@Service
public class TeacherServiceImp implements TeacherService {
    private final TeacherRepository teacherRepository;
    private final StandardRepository standardRepository;

    public TeacherServiceImp(TeacherRepository teacherRepository, StandardRepository standardRepository) {
        this.teacherRepository = teacherRepository;
        this.standardRepository = standardRepository;
    }

    @Override
    public TeacherResDTO createTeacher(TeacherReqDTO teacherreq) {
        Teacher teacher = new Teacher();
        teacher.setTeacherName(teacherreq.getTeacherName());
        teacher.setTeacherType(teacherreq.getTeacherType());
        Standard standardgetting = standardRepository.findByStandardName(teacherreq.getStandard());
        teacher.setStandard(standardgetting);
        Teacher teachers = teacherRepository.save(teacher);

        TeacherResDTO teacherResDTO = new TeacherResDTO();
        teacherResDTO.setTeacherName(teachers.getTeacherName());

        return teacherResDTO;
    }

    @Override
    public TeacherUpResDTO editTeacher(TeacherUpReqDTO teacherreq) {
        Optional<Teacher> teacherGet = teacherRepository.findById(teacherreq.getTeacherId());
        if (teacherGet.isPresent()) {
            teacherGet.get().setTeacherName(teacherreq.getTeacherName());
            teacherGet.get().setTeacherType(teacherreq.getTeacherType());
            Standard standardgetting = standardRepository.findByStandardName(teacherreq.getStandard());

            if (Objects.isNull(standardgetting)) {
                throw new BadDataException("course not found");
            }
            teacherGet.get().setStandard(standardgetting);
            Teacher saveco = teacherRepository.save(teacherGet.get());
            TeacherUpResDTO teacherUpResDTO = new TeacherUpResDTO();
            teacherUpResDTO.setTeacherType(saveco.getTeacherType());
            teacherUpResDTO.setTeacherName(saveco.getTeacherName());
            return teacherUpResDTO;
        } else {
            throw new BadDataException(" course found!");
        }
    }

    @Override
    public TeacherListResDTO listAllTeacher(String query, String filterBy, String filtervalue, Integer page, Integer size) {
        TeacherListResDTO listResponse = new TeacherListResDTO();
        Page<Teacher> teachers;
        List<TeacherProfileListDTO> teacherLists = new ArrayList<>();

        String fieldSort = "teacherName";
        Integer currentPage = page;
        Integer pageSize = size;
        Sort.Order sortOrder = new Sort.Order(Sort.Direction.ASC, fieldSort);
        PageRequest pageRequest = PageRequest.of(currentPage, pageSize, Sort.by(sortOrder));
        teachers = teacherRepository.findAll(pageRequest);


        if (teachers != null && teachers.getContent().size() > 0) {
            teachers.getContent().forEach(userObj -> {
                        TeacherProfileListDTO teacherProfileListDTO = getPaginatedTeacherList(userObj);
                        if (teacherProfileListDTO != null) {
                            teacherLists.add(teacherProfileListDTO);
                        }
                    }
            );
        }
        if (teacherLists.size() > 0) {
            listResponse.setCurrentPages(page);
            listResponse.setTotalPages(teachers.getTotalPages());
            listResponse.setTeacherProfileList(teacherLists);
            listResponse.setTotal(teachers.getTotalElements());

            return listResponse;
        } else {
            listResponse.setCurrentPages(0);
            listResponse.setTotalPages(0);
            listResponse.setTeacherProfileList(teacherLists);
            listResponse.setTotal(0L);
            return listResponse;


        }
    }


    private TeacherProfileListDTO getPaginatedTeacherList(Teacher userObj) {
        TeacherProfileListDTO teacherProfileListDTO = new TeacherProfileListDTO();
        teacherProfileListDTO.setTeacherName(userObj.getTeacherName());
        teacherProfileListDTO.setStandard(userObj.getStandard().getStandardName());
        teacherProfileListDTO.setTeacherType(userObj.getTeacherType());

//        courseProfileList.setTeacher(userObj.getTeacher().getTeacherName());
        return teacherProfileListDTO;
    }

    @Override
    public TeacherIDDetailsDTO getTeacherDetails(Long studentId) {
        TeacherIDDetailsDTO teacherIDDetails = new TeacherIDDetailsDTO();
        Optional<Teacher> teacher = teacherRepository.findById(studentId);

        if (teacher.isPresent()) {
            teacherIDDetails.setTeacherId(teacher.get().getTeacherId());
            teacherIDDetails.setTeacherName(teacher.get().getTeacherName());
//            teacherIDDetails.setStandard(teacher.get().getStandard());
            teacherIDDetails.setTeacherType(teacher.get().getTeacherType());
        }
        return teacherIDDetails;
    }

    @Override
    public TeacherStatusDTO deleteTeacherId(Long teacherExternalId) {

        if (teacherExternalId !=0) {
            Optional<Teacher> teacherGet = teacherRepository.findByTeacherIdAndDeletedFalse(teacherExternalId);
            if (teacherGet.isPresent()) {
                teacherGet.get().setDeleted(true);
                teacherRepository.save(teacherGet.get());


                TeacherStatusDTO response = new TeacherStatusDTO();
                response.setStatus("DELETED");
                return response;
            } else {
                throw new BadDataException("There is no teacher corresponding externalId");
            }
        } else {
            throw new BadDataException("Please provide teacher external id");
        }
    }
    }



