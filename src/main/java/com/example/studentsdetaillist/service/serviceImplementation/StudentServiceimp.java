package com.example.studentsdetaillist.service.serviceImplementation;
import com.example.studentsdetaillist.Exception.BadDataException;
import com.example.studentsdetaillist.dto.*;
import com.example.studentsdetaillist.entity.*;
import com.example.studentsdetaillist.repository.*;
import com.example.studentsdetaillist.service.StudentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.hibernate.event.internal.EntityState.DELETED;


@Service
public class StudentServiceimp implements StudentService {
    private final CourseRepository courseRepository;
    private final StandardRepository standardRepository;
    private final StudentAddressRepository studentAddressRepository;
    private final StudentCourseRepository studentCourseRepository;
    private final StudentRepository studentRepository;


    public StudentServiceimp(CourseRepository courseRepository,
                             StandardRepository standardRepository,
                             StudentAddressRepository studentAddressRepository,
                             StudentCourseRepository studentCourseRepository,
                             StudentRepository studentRepository) {
        this.courseRepository = courseRepository;
        this.standardRepository = standardRepository;
        this.studentAddressRepository = studentAddressRepository;
        this.studentCourseRepository = studentCourseRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    public StudentResDTO createStudent(StudentReqDTO studentreq) {
        Student student = new Student();
        student.setStudentName(studentreq.getStudentName());
        student.setRowVersion(studentreq.getRowVersion());
        Standard standardss = standardRepository.findByStandardName(studentreq.getStandard());
        student.setStandard(standardss);
        Student students = studentRepository.save(student);
        StudentAddress studentAddress = new StudentAddress();

        studentAddress.setCity(studentreq.getCity());
        studentAddress.setState(studentreq.getState());
        studentAddress.setAddress1(studentreq.getAddress1());
        studentAddress.setAddress2(studentreq.getAddress2());
        studentAddress.setStudent(students);

        studentAddressRepository.save(studentAddress);
        StudentCourse studentCourse = new StudentCourse();
        studentCourse.setStudent(students);
        Course course = courseRepository.findByCourseName(studentreq.getCourse());
        studentCourse.setCourse(course);
        studentCourseRepository.save(studentCourse);
        StudentResDTO studentResDTO = new StudentResDTO();
        studentResDTO.setStudentId(students.getStudentId());

        return studentResDTO;

    }

    @Override
    public StudentUpResDTO editStudent(StudentUpReqDTO request) {
        Optional<Student> studentGet = studentRepository.findById(request.getStudentId());
        if (studentGet.isPresent()) {
            studentGet.get().setStudentName(request.getStudentName());
            studentGet.get().setRowVersion(request.getRowversion());

            Standard standardGetting = standardRepository.findByStandardName(request.getStandard());
            if (Objects.isNull(standardGetting)) {
                throw new BadDataException("Standard not found");
            }

            studentGet.get().setStandard(standardGetting);
            Student saveToDb = studentRepository.save(studentGet.get());

            //student address insert
            Optional<StudentAddress> studentAddressGet = studentAddressRepository.findByStudent(studentGet.get());
            studentAddressGet.get().setAddress1(request.getAddress1());
            studentAddressGet.get().setAddress2(request.getAddress2());
            studentAddressGet.get().setCity(request.getCity());
            studentAddressGet.get().setState(request.getState());
            studentAddressGet.get().setStudent(saveToDb);
            studentAddressRepository.save(studentAddressGet.get());

            Course courseGetting = courseRepository.findByCourseName(request.getCourse());
            if (Objects.isNull(courseGetting)) {
                throw new BadDataException("Course not found");
            }

            //student course insert
            Optional<StudentCourse> studentCourseGet = studentCourseRepository.findByStudent(studentGet.get());
            studentCourseGet.get().setStudent(saveToDb);
            studentCourseGet.get().setCourse(courseGetting);

            studentCourseRepository.save(studentCourseGet.get());


            StudentUpResDTO response = new StudentUpResDTO();
            response.setStudentName(saveToDb.getStudentName());
            response.setRowVersion(saveToDb.getRowVersion());
            return response;

        } else {
            throw new BadDataException("Student not found!");
        }

    }

    @Override
    public StudentsListResDTO listAllStudents(String query, String filterBy, String filterValue, Integer page, Integer size) {

        StudentsListResDTO listResponse = new StudentsListResDTO();
        Page<Student> student;
        List<StudentProfileListDTO> studentLists = new ArrayList<>();

        String fieldSort = "studentName";
        Integer currentPage = page;
        Integer pageSize = size;
        Sort.Order sortOrder = new Sort.Order(Sort.Direction.ASC, fieldSort);
        PageRequest pageRequest = PageRequest.of(currentPage, pageSize, Sort.by(sortOrder));
        student = studentRepository.findAll(pageRequest);


        if (student != null && student.getContent().size() > 0) {
            student.getContent().forEach(userObj -> {
                        StudentProfileListDTO studentProfileList = getPaginatedUserList(userObj);
                        if (studentProfileList != null) {
                            studentLists.add(studentProfileList);
                        }
                    }
            );
        }
        if (studentLists.size() > 0) {
            listResponse.setCurrentPages(page);
            listResponse.setTotalPages(student.getTotalPages());
            listResponse.setStudentProfileList(studentLists);
            listResponse.setTotal(student.getTotalElements());

            return listResponse;
        } else {
            listResponse.setCurrentPages(0);
            listResponse.setTotalPages(0);
            listResponse.setStudentProfileList(studentLists);
            listResponse.setTotal(0L);
            return listResponse;
        }
    }

    private StudentProfileListDTO getPaginatedUserList(Student userObj) {
        StudentProfileListDTO studentProfileList = new StudentProfileListDTO();
        studentProfileList.setStudentName(userObj.getStudentName());
        studentProfileList.setRowVersion(userObj.getRowVersion());
        studentProfileList.setStandard(userObj.getStandard().getStandardName());

        StudentAddress studentAddress = studentAddressRepository.findByAddress(userObj.getStudentId());

        studentProfileList.setAddress1(studentAddress.getAddress1());
        studentProfileList.setAddress2(studentAddress.getAddress2());
        studentProfileList.setCity(studentAddress.getCity());

        StudentCourse studentCourse = studentCourseRepository.findByCourseId(userObj.getStudentId());
        studentProfileList.setCourse(studentCourse.getCourse().getCourseName());
        return studentProfileList;
    }


    @Override
    public StudentsIDDetailsDTO getStudentsDetails(Long studentId) {
        StudentsIDDetailsDTO studentsIDDetails = new StudentsIDDetailsDTO();
        Optional<Student> student = studentRepository.findById(studentId);

        if (student.isPresent()) {
            studentsIDDetails.setStudentId(student.get().getStudentId());
            studentsIDDetails.setStudentName(student.get().getStudentName());
            studentsIDDetails.setStandard(student.get().getStandard().getStandardName());
            studentsIDDetails.setRowVersion(student.get().getRowVersion());

            StudentAddress studentAddress = studentAddressRepository.findByAddress(student.get().getStudentId());
            studentsIDDetails.setAddress1(studentAddress.getAddress1());
            studentsIDDetails.setAddress2(studentAddress.getAddress2());

            StudentCourse studentCourse = studentCourseRepository.findByCourseId(student.get().getStudentId());
            studentsIDDetails.setCourse(studentCourse.getCourse().getCourseName());

        }
        return studentsIDDetails;
    }


    @Override
    public List<StudentSearchResDTO> findStudentByCriteria(int page, int size, Sort.Direction sort, String search, String city, String standard) {
        if (search==null){
            throw new BadDataException("please enter student name");

       }
        Page<Student> students;
        PageRequest pageRequestQuery = PageRequest.of(page,size);
        Page<Student> studentNames = studentRepository.findBySearches(search,city,standard,pageRequestQuery);
        List<StudentSearchResDTO> studentSearchResponse = new ArrayList<>();
        List<Student> studentSearchs = new ArrayList<>();
        studentSearchs.addAll(studentNames.getContent().stream().filter(distinctByKey(p -> (p.getStudentId())))
                .collect(Collectors.toList()));
             if(studentSearchs !=null && studentSearchs.size()>0){
               studentSearchs.forEach(studentObj -> {
                StudentSearchResDTO studentSearchResp = paginatedStudentList(studentObj);
                studentSearchResponse.add(studentSearchResp);

                 });
            } else {
            throw new BadDataException("no content found");
         }
         return studentSearchResponse;
         }
         /**
     * @return paginatedStudentList
     */

    public static StudentSearchResDTO paginatedStudentList (final Student studentObj){
        StudentSearchResDTO studentSearchResp = new StudentSearchResDTO();
        studentSearchResp.setStudentId(studentObj.getStudentId());
        studentSearchResp.setStudentName(studentObj.getStudentName());
//        studentSearchResp.setStandard(studentObj.getStandard());
        return studentSearchResp;
    }

    public  static <T>Predicate<T> distinctByKey(final Function<? super T,Object> KeyExtractor){
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(KeyExtractor.apply(t), Boolean.TRUE)==null;
    }

    @Override
    public List<CourseSearchResDTO> findStudentByCourse(int page, int size, Sort.Direction sort,String courseName) {
        if (courseName==null){
            throw new BadDataException("please enter student name");

        }
        Page<StudentCourse> students;
        PageRequest pageRequestQuery = PageRequest.of(page,size);
        Page<StudentCourse> studentNames = studentCourseRepository.findStudentByCourse(courseName,pageRequestQuery);
        List<CourseSearchResDTO> courseSearchResponse = new ArrayList<>();
        List<StudentCourse> studentSearches = new ArrayList<>();
        studentSearches.addAll(studentNames.getContent().stream().filter(distinctByKey(p -> (p.getStudent().getStudentId())))
                .collect(Collectors.toList()));
        if(studentSearches !=null && studentSearches.size()>0){
            studentSearches.forEach(studentObj -> {
                CourseSearchResDTO courseSearchResp = paginatedCourseList(studentObj);
                courseSearchResponse.add(courseSearchResp);

            });
        } else {
            throw new BadDataException("no content found");
        }
        return courseSearchResponse;
    }
    /**
     * @return paginatedStudentList
     */

    public static CourseSearchResDTO paginatedCourseList (final StudentCourse studentObj){
        CourseSearchResDTO studentSearchResp = new CourseSearchResDTO();
        studentSearchResp.setStudentId(studentObj.getStudent().getStudentId());
        studentSearchResp.setStudentName(studentObj.getStudent().getStudentName());
        studentSearchResp.setCourse(studentObj.getCourse().getCourseName());

        return studentSearchResp;
    }

    @Override
    public StudentStatusDTO deleteStudent(String addressExternalId) {
        return null;
    }

    /**
     *
     * @return deleteAddress.
     */
    @Override
    public StudentStatusDTO deleteStudentId(final Long studentExternalId) {

        if (studentExternalId !=0) {
            Optional<Student> studentGet = studentRepository.findByStudentIdAndDeletedFalse(studentExternalId);
            if (studentGet.isPresent()) {
                studentGet.get().setDeleted(true);
                studentRepository.save(studentGet.get());


                StudentStatusDTO response = new StudentStatusDTO();
                response.setStatus("DELETED");
                return response;
            } else {
                throw new BadDataException("There is no address corresponding externalId");
            }
        } else {
            throw new BadDataException("Please provide address external id");
        }
    }
}
