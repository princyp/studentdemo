package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class StudentsIDDetailsDTO {
    private String StudentName;
    private Long StudentId;
    private String Standard;

    private String Address1;

    private String Address2;

    private String RowVersion;

    private String City;

    private String State;

    private String Course;

}
