package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class CourseIDDetailsDTO {
    private String location;
    private String teacher;
    private String courseName;
    private String standName;
    private long courseId;

}
