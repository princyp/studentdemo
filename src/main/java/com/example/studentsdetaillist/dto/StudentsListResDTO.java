package com.example.studentsdetaillist.dto;

import lombok.Data;

import java.util.List;
@Data
public class StudentsListResDTO {
    private Long total;
    private Integer totalPages;
    private Integer currentPages;


    private List<StudentProfileListDTO> studentProfileList;
}
