package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class StudentStatusDTO {
    private String status;
}
