package com.example.studentsdetaillist.dto;

import lombok.Data;

import java.util.List;


@Data
public class TeacherListResDTO {
    private Long total;
    private Integer totalPages;
    private Integer currentPages;



    private List<TeacherProfileListDTO> teacherProfileList;
}
