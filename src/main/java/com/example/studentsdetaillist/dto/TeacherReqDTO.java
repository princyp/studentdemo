package com.example.studentsdetaillist.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * TeacherReqDTO.
 */
@Data
public class TeacherReqDTO {

    @NotNull(message = "First name must not be null")
    @NotEmpty(message = "First name must not be empty")
    private String teacherName;

//    @NotNull(message = "StandardId must not be null")
//    @NotEmpty(message = "StandardId must not be empty")
    private String standard;

//    @NotNull(message = "TeacherType must not be null")
//    @NotEmpty(message = "TeacherType must not be empty")
    private String teacherType;




}


