package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class TeacherProfileListDTO {
    private String teacherName;
    private String standard;
    private String teacherType;
}
