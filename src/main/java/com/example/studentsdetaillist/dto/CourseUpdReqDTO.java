package com.example.studentsdetaillist.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CourseUpdReqDTO {
    @NotNull(message = "")
    @NotEmpty(message = "")
    private String courseName;

    @NotNull(message = "")
    @NotEmpty(message = "")
    private String location;
    private String teacher;
    private Long courseId;

}
