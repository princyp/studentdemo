package com.example.studentsdetaillist.dto;

import lombok.Data;

/**
 * courseUpdateResponseDTO.
 */
@Data
public class CourseUpdResDTO {
    private String courseName;
    private Long courseId;


}
