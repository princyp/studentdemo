package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class TeacherResDTO {
    private String TeacherName;
    private String Standard;
}
