package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class StudentUpResDTO {
    private String studentName;

    private  String rowVersion;
}
