package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class CourseProfileListDTO {
    private String location;
    private String teacher;
    private String courseName;
    private String standName;


}
