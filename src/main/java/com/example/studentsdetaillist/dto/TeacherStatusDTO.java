package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class TeacherStatusDTO {
    private String status;
}
