package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class StudentSearchResDTO {
    private Long studentId;
    private String studentName;
    private String standard;
    private String search;
    private String city;

}
