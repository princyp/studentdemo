package com.example.studentsdetaillist.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class StudentReqDTO {
    @NotNull(message = "StudentName must not be null")
    @NotEmpty(message = "StudentName name must not be empty")
    private String StudentName;

    @NotNull(message = "StandardId must not be null")
   @NotEmpty(message = "StandardId must not be empty")
    private String Standard;


       @NotNull(message = "Address1 must not be null")
     @NotEmpty(message = "Address1 must not be empty")
       private String Address1;
       private String Address2;
      private String RowVersion;
      private String City;
      private String State;
    private String Course;




}
