package com.example.studentsdetaillist.dto;

import lombok.Data;

import java.util.List;

@Data
public class CourseListResDTO {
    private Long total;
    private Integer totalPages;
    private Integer currentPages;



    private List<CourseProfileListDTO> courseProfileList;

}
