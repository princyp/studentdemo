package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class CourseSearchResDTO {
    private Long studentId;
    private String course;

    private String studentName;

}
