package com.example.studentsdetaillist.dto;

import lombok.Data;
/**
 * TeacherUpResDTO.
 */

@Data
public class TeacherUpResDTO {
    private String teacherName;

    private  String teacherType;
}
