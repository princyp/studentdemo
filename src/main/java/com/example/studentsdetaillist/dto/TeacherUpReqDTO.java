package com.example.studentsdetaillist.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TeacherUpReqDTO {
    @NotNull(message = "")
    @NotEmpty(message = "")
    private String teacherName;

    @NotNull(message = "")
    @NotEmpty(message = "")
    private String standard;

    private Long teacherId;
    private  String teacherType;
}
