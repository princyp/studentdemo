package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class TeacherIDDetailsDTO {
    private String teacherName;
    private String standard;
    private String teacherType;
    private Long teacherId;

}


