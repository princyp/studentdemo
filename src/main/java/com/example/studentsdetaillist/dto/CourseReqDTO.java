package com.example.studentsdetaillist.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
@Data
public class CourseReqDTO {
    @NotNull(message = "Location must not be null")
    @NotEmpty(message = "Location name must not be empty")
    private String location;

    //    @NotNull(message = "StandardId must not be null")
//    @NotEmpty(message = "StandardId must not be empty")
    private String teacher;


    //    @NotNull(message = "TeacherType must not be null")
//    @NotEmpty(message = "TeacherType must not be empty")
    private String courseName;


}
