package com.example.studentsdetaillist.dto;

import lombok.Data;

@Data
public class CourseStatusDTO {
    private String status;
}
