package com.example.studentsdetaillist.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
@Data
public class StudentUpReqDTO {
    @NotNull(message = "")
    @NotEmpty(message = "")
    private Long studentId;

    @NotNull(message = "")
    @NotEmpty(message = "")
    private String studentName;

    private String standard;
    private  String rowversion;
    private  String address1;
    private  String address2;
    private  String city;
    private  String state;
    private  String course;

}

