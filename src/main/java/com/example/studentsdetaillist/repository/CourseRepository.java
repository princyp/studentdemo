package com.example.studentsdetaillist.repository;

import com.example.studentsdetaillist.entity.Course;
import com.example.studentsdetaillist.entity.Student;
import com.example.studentsdetaillist.entity.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course,Long> {
  Course  findByCourseName(String course);
  @Query(value="select * from course a where a.course_id= :id", nativeQuery=true)
  Optional<Course> findByCourseId(Long id);
  Page<Course> findAll(Pageable pageable);
  Optional<Course> findByCourseIdAndDeletedFalse(Long id);

}
