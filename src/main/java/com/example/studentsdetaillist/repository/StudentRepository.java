package com.example.studentsdetaillist.repository;

import com.example.studentsdetaillist.entity.Course;
import com.example.studentsdetaillist.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student,Long> {
    Optional<Student> findById(Long id);
    Page<Student> findAll(Pageable pageable);
    @Query(value = "SELECT distinct s1.* " +
            "FROM student s1 inner join standard st " +
            "on st.standard_id=s1.standard_id inner join student_address sd " +
            "on sd.student_id = s1.student_id WHERE s1.student_name LIKE %?1% " +
            "and st.standard_name LIKE %?3%  and sd.city LIKE  %?2% ",nativeQuery = true)

    Page<Student> findBySearches(String search,String city,String standard,Pageable pageable);
    Optional<Student> findByStudentIdAndDeletedFalse(Long id);

}
