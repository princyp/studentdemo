package com.example.studentsdetaillist.repository;

import com.example.studentsdetaillist.entity.Student;
import com.example.studentsdetaillist.entity.StudentAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface StudentAddressRepository extends JpaRepository<StudentAddress,Long> {

    Optional<StudentAddress> findByStudent(Student studentId);
    @Query(value="select * from student_address a where a.student_id= :id", nativeQuery=true)
    StudentAddress findByAddress(Long id);
//    StudentAddress findByAddressByStudent(Long StudentId);
}
