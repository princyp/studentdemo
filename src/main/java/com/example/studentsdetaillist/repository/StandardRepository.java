package com.example.studentsdetaillist.repository;

import com.example.studentsdetaillist.entity.Standard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StandardRepository extends JpaRepository<Standard,Long>{
Standard findByStandardName(String name);

}
