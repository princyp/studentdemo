package com.example.studentsdetaillist.repository;

import com.example.studentsdetaillist.entity.Student;

import com.example.studentsdetaillist.entity.StudentCourse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface StudentCourseRepository extends JpaRepository<StudentCourse,Long> {

    Optional<StudentCourse> findByStudent(Student studentId);


    @Query(value="select * from student_course a where a.student_id= :studentId", nativeQuery=true)
    StudentCourse findByCourseId(Long studentId);


    @Query(value="select * from student_course sc inner join course c on sc.course_id=c.course_id where c.course_name=?1", nativeQuery=true)
    Page<StudentCourse> findStudentByCourse(String courseName, Pageable pageable);
}
