package com.example.studentsdetaillist.repository;
import com.example.studentsdetaillist.entity.Course;
import com.example.studentsdetaillist.entity.Student;
import com.example.studentsdetaillist.entity.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface TeacherRepository extends JpaRepository<Teacher,Long> {
    Teacher findByTeacherName(String name);

    Optional<Teacher> findById(Long id);
    Page<Teacher> findAll(Pageable pageable);
    Optional<Teacher> findByTeacherIdAndDeletedFalse(Long id);
}
