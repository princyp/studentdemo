package com.example.studentsdetaillist.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * The Student Entity.
 */
@Data
@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long studentId;
    private String studentName;
    private String rowVersion;

    private boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn (name = "standardId")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Standard standard;

//    @OneToMany(mappedBy = "standardaddress", fetch = FetchType.LAZY)
//    @JsonBackReference
//    @EqualsAndHashCode.Exclude
//    private Set<StudentAddress> standardaddress =new HashSet<>();


}

