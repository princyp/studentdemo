package com.example.studentsdetaillist.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long teacherId;

    private String teacherName;
    private String teacherType;
    private boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn (name = "standardId")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Standard standard;

//    @OneToMany(mappedBy = "Course", fetch = FetchType.LAZY)
//    @JsonBackReference
//    @EqualsAndHashCode.Exclude
//    private Set<Course> course =new HashSet<>();



}
