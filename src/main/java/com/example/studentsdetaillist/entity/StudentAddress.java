package com.example.studentsdetaillist.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
public class StudentAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String address1;
    private String address2;
    private String city;
    private String state;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn (name = "studentId")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Student student;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }


//    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY)
//    @JsonBackReference
//    @EqualsAndHashCode.Exclude
//    private Set<Student> student =new HashSet<>();




}

