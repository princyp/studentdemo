package com.example.studentsdetaillist.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Standard{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long standardId;
    private String standardName;
    private String description;

//
//    @OneToMany(mappedBy = "teacher", fetch = FetchType.LAZY)
//    @JsonBackReference
//    @EqualsAndHashCode.Exclude
//    private Set<Teacher> teacher =new HashSet<>();




}
